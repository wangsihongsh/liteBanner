
## liteBanner
liteBanner是一个轻量级的本地违禁词检测（素材引用github），整句过滤的Jar包

* 自带违禁词词库，整包只有100多K，包含1w多的违禁词
* 可以输出过滤后的字符串
* 可以自动检测出带空格，加分隔符的违禁词
* 1秒可以过滤30w条的词句

```java

	// 加载默认词典
	BannerFilter filter = BannerFilter.DEFAULT;
	// 向过滤器增加一个词，额外造个词
	filter.put("AB违禁词");
	
	String sentence = "我是一个T M D(他妈的) 法轮功AB违禁词，追杀本拉登";
	BannerResp resp = filter.process(sentence);
	System.out.println(resp);

```
返回
```java
	BannerResp [hasSensitiveWords=true, sensitiveWords=[TMD, 他妈的, 法轮功, AB违禁词, 本拉登], filterStr=我是一个***(***)********，追杀***]
```