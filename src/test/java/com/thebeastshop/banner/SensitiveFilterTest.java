package com.thebeastshop.banner;

import junit.framework.TestCase;

public class SensitiveFilterTest extends TestCase{
	
	public void test() throws Exception{
		// 加载默认词典
		BannerFilter filter = BannerFilter.DEFAULT;
		// 向过滤器增加一个词，额外造个词
		filter.put("AB违禁词");
		
		String sentence = "我是一个T M D(他妈的) 法轮功AB违禁词，追杀本拉登";
		BannerResp resp = filter.process(sentence);
		System.out.println(resp);
	}
}
